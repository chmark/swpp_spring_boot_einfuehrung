package com.example.testumgebung;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestumgebungApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestumgebungApplication.class, args);
    }

}
