package com.example.testumgebung.Config;

import com.example.testumgebung.Entities.Student;
import com.example.testumgebung.Repositories.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class StudentConfig {

    @Autowired
    private final StudentRepo studentRepo;

    public StudentConfig(StudentRepo studentRepo) {
        this.studentRepo = studentRepo;
    }

    @Bean
    CommandLineRunner commandLineRunner(){
        return args -> {
            Student student1 = new Student(
                    "Christoph",
                    "Mark",
                    "christophmark93@gmail.com",
                    123,
                    "123",
                    null
            );
            Student student2 = new Student(
                    "Felix",
                    "Spiss",
                    "spiss.felix@gmail.com",
                    123,
                    "123",
                    null
            );

            if(studentRepo.findAll().size() == 0) {
                studentRepo.saveAll(
                        List.of(student1, student2)
                );
            }
        };
    }
}

