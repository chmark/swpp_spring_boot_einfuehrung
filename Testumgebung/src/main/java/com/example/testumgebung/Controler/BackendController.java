package com.example.testumgebung.Controler;

import com.example.testumgebung.Request.LerngruppeRequest;
import com.example.testumgebung.Request.LoginRequest;
import com.example.testumgebung.Request.StudentRequest;
import com.example.testumgebung.Service.LerngruppeService;
import com.example.testumgebung.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/test/v1")
public class BackendController {

    @Autowired
    private final StudentService studentService;
    private final LerngruppeService lerngruppeService;

    public BackendController(StudentService studentService, LerngruppeService lerngruppeService) {
        this.studentService = studentService;
        this.lerngruppeService = lerngruppeService;
    }

    @PostMapping(path = "/registrieren")
    public String registrieren(@RequestBody StudentRequest studentRequest){
        return studentService.registrieren(studentRequest);
    }

    @PostMapping(path = "/login")
    public String login(@RequestBody LoginRequest loginRequest){
        boolean existiertStudent = studentService.login(loginRequest.getEmail(), loginRequest.getPasswort()).isPresent();
        if(existiertStudent){
            return "Anmeldung erfolgreich abgeschlossen";
        }
        throw  new IllegalStateException("Nutzer konnte nicht gefunden werden");
    }

    @PostMapping(path = "/erzeugeGruppe/{id}")
    public String erzeugerGruppe(@RequestBody LerngruppeRequest lerngruppeRequest, @PathVariable int id){
        return lerngruppeService.erzeugeLerngruppe(lerngruppeRequest, id);
    }

    @PostMapping(path = "/hinzufügen/{gruppenId}/{studentenId}")
    public String studentHinzufügen(@PathVariable int gruppenId, @PathVariable int studentenId){
        return lerngruppeService.studentHinzufügen(gruppenId, studentenId);
    }
}
